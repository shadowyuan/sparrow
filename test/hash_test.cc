#include <string>

#include <gtest/gtest.h>

#include "sparrow/hash/sha1.h"
#include "sparrow/hash/md5.h"

static std::string HashResultToReadableString(char *input, int32_t len) {
    std::string result;
    result.reserve(2 * len);

    for (int32_t i = 0; i < len; i++) {
        char temp[8] = {0};
        int bytes = snprintf(temp, sizeof(temp), "%02X", input[i]);
        result.append(temp, bytes);
    }
    return result;
}

TEST(HashTest, sha1_test) {
    // SHA1:DB8AC1C259EB89D4A131B253BACFCA5F319D54F2
    char buff[] = "HelloWorld";
    char result[SHA1_DIGEST_SIZE] = {0};
    struct sha1_ctx ctx;
    sha1_init_ctx(&ctx);
    sha1_process_bytes(&ctx, buff, strlen(buff));
    sha1_finish_ctx(&ctx, result);

    std::string str = HashResultToReadableString(result, SHA1_DIGEST_SIZE);
    EXPECT_EQ(str, "DB8AC1C259EB89D4A131B253BACFCA5F319D54F2");
}

TEST(HashTest, md5_test) {
    // MD5:68E109F0F40CA72A15E05CC22786F8E6
    char buff[] = "HelloWorld";
    char result[MD5_DIGEST_SIZE] = {0};
    struct md5_ctx ctx;
    md5_init_ctx(&ctx);
    md5_process_bytes(&ctx, buff, strlen(buff));
    md5_finish_ctx(&ctx, result);

    std::string str = HashResultToReadableString(result, MD5_DIGEST_SIZE);
    EXPECT_EQ(str, "68E109F0F40CA72A15E05CC22786F8E6");
}

int main(int argc, char *argv[]) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
