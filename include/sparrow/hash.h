//
// Copyright 2023 The Sparrow Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef SPARROW_INCLUDE_HASH_H_
#define SPARROW_INCLUDE_HASH_H_

#include <stddef.h>
#include <stdint.h>
#include <string.h>

#include <array>
#include <string>

#include "sparrow/impl/hash/murmur_hash.h"
namespace sparrow {

/* compute the hash of key (length len) */
inline uint32_t hash32(const void *key, int32_t len, uint32_t seed = 0) {
    return murmur_hash_32(key, len, seed);
}
inline uint32_t hash32(const char *data, uint32_t seed = 0) {
    return murmur_hash_32(data, strlen(data), seed);
}
inline uint32_t hash32(const std::string &s, uint32_t seed = 0) {
    return murmur_hash_32(s.data(), s.size(), seed);
}
template <size_t SIZE>
inline uint32_t hash32(const char (&buff)[SIZE], uint32_t seed = 0) {
    return murmur_hash_32(&buff[0], SIZE, seed);
}
template <size_t SIZE>
inline uint32_t hash32(const std::array<char, SIZE> &buff, uint32_t seed = 0) {
    return murmur_hash_32(buff.data(), SIZE, seed);
}
} // namespace sparrow
#endif /* SPARROW_INCLUDE_HASH_H_ */
