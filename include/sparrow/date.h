//
// Copyright 2022 The Sparrow Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef SPARROW_DATE_H_
#define SPARROW_DATE_H_

#include <stdint.h>
#include <time.h>
#include <functional>
#include <string>

#include "sparrow/impl/time.h"
namespace sparrow {

// format '2022-08-29 19:46:09'
inline std::string DateTimeString() {
    char buff[32] = {0};
    int len = sp_format_time("%F %T", buff);
    return std::string(buff, len);
}

// format 'Mon Aug 29 19:35:54 2022'
inline std::string DateTimeFriendly() {
    char buff[32] = {0};
    int len = sp_format_time("%c", buff);
    return std::string(buff, len);
}

// format '2022-08-29'
inline std::string DateString() {
    char buff[32] = {0};
    int len = sp_format_time("%F", buff);
    return std::string(buff, len);
}

// format '08/29/22'
inline std::string ShortDateString() {
    char buff[32] = {0};
    int len = sp_format_time("%D", buff);
    return std::string(buff, len);
}
// 	format '14:55:02'
inline std::string TimeString24Hour() {
    char buff[32] = {0};
    int len = sp_format_time("%X", buff);
    return std::string(buff, len);
}

// format '02:55:02 pm'
inline std::string TimeString12Hour() {
    char buff[32] = {0};
    int len = sp_format_time("%r", buff);
    return std::string(buff, len);
}

// Returns the current milliseconds time
inline int64_t GetCurrentMilliseconds() {
    struct sp_time_sepc_t ts;
    sp_system_clock(&ts);
    return SP_TIMESPEC_MILLI(ts);
}

// Returns the realtime nanoseconds time of the system
inline int64_t GetRealTimeNanoseconds() {
    struct sp_time_sepc_t ts;
    sp_system_clock(&ts);
    return SP_TIMESPEC_NANOS(ts);
}

// Returns the nanoseconds time of steady clock
inline int64_t GetMonotonicNanoseconds() {
    struct sp_time_sepc_t ts;
    sp_steady_clock(&ts);
    return SP_TIMESPEC_NANOS(ts);
}

} // namespace sparrow
#endif // SPARROW_DATE_H_
