//
// Copyright 2023 The Sparrow Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef SPARROW_IMPL_FORMAT_H_
#define SPARROW_IMPL_FORMAT_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "sparrow/impl/export.h"
#include "sparrow/impl/types.h"

SPARROW_API struct FlexibleArray *sp_printf(const char *format, ...);

/**
 * @brief split string
 *
 * @param target target string
 * @param delimiters characters used for split
 * @param callback callback function use for receive data
 * @param usr pass to callback function
 * @return If it fails, return -1, otherwise return the number of characters after split
 */
SPARROW_API int sp_string_split(const char *target, const char *delimiters,
                                void (*callback)(const char *str, void *usr), void *usr);

SPARROW_API int sp_string_trim(const char *input, int input_size, char *output);

/**
 * @brief Convert a sequence of bytes to a sequence of characters in hexadecimal
 * @param input Input byte sequence
 * @param input_size the bytes of the input sequence
 * @param output The output byte sequence, allocated and released by the caller,
 *               it must be two times of input_size
 * @param upper_case 1 uppercase, 0 lowercase
 *
 * @return Returns the number of bytes copied to output
 *
 * @Example: 0x23 -> '2','3' return 2
 */
SPARROW_API int sp_hex_encode(const char *input, int input_size, char *output, int upper_case);

/**
 * @brief Convert a sequence of characters in hexadecimal to a sequence of bytes
 * @param input Input a sequence of characters in hexadecimal
 * @param input_size the bytes of the input sequence
 * @param output The output byte sequence, allocated and released by the caller,
 *               it must be greater than or equal to 1/2 of input_size
 *
 * @return Returns the number of bytes copied to output
 *
 * @Example: "23" -> 0x23 return 1
 */
SPARROW_API int sp_hex_decode(const char *input, int input_size, char *output);

#ifdef __cplusplus
} /* end extern "C" */
#endif
#endif // SPARROW_IMPL_FORMAT_H_
