//
// Copyright 2022 The Sparrow Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef SPARROW_IMPL_HASH_MURMUR_HASH_H_
#define SPARROW_IMPL_HASH_MURMUR_HASH_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>

#include "sparrow/impl/export.h"

/* compute the hash of key (length len) */
SPARROW_API uint32_t murmur_hash_32(const void *key, int len, uint32_t seed);
SPARROW_API uint64_t murmur_hash_64(const void *key, int len, uint64_t seed);

#ifdef __cplusplus
} /* end extern "C" */
#endif

#endif /* SPARROW_IMPL_HASH_MURMUR_HASH_H_ */
