//
// Copyright 2023 The Sparrow Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef SPARROW_IMPL_HASH_SHA256_H_
#define SPARROW_IMPL_HASH_SHA256_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>

#include "sparrow/impl/export.h"

#define SHA256_DIGEST_SIZE 32

struct sha256_ctx {
    uint32_t H[8];
    uint64_t Count;
    uint8_t Buffer[64];
};
/**
 * @brief Allocate the sha256_ctx object and initialize it
 *
 * @return sha256_ctx object pointer, needs to be released
 *         by call sha256_done/sha256_free
 *
 */
SPARROW_API void sha256_init(struct sha256_ctx *ctx);

SPARROW_API void sha256_process(struct sha256_ctx *ctx, const void *data, size_t size);

/**
 * @brief Obtain the final sha256 result
 *
 * @param ctx sha256 context
 * @param digest 32-byte array for storing sha256 results
 *
 * @note the ctx pointer will be released and must not be reused.
 */
SPARROW_API uint8_t *sha256_done(struct sha256_ctx *ctx, uint8_t *digest);

#ifdef __cplusplus
} /* end extern "C" */
#endif

#endif // SPARROW_IMPL_HASH_SHA256_H_
