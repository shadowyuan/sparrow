//
// Copyright 2023 The Sparrow Authors.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      https://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#ifndef SPARROW_IMPL_BASE64_H_
#define SPARROW_IMPL_BASE64_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "sparrow/impl/export.h"
#include "sparrow/impl/types.h"

// Encodes the input string in base64.
SPARROW_API void Base64Encode(const void *input, int input_size, struct FlexibleArray **output);

// Decodes the base64 input string.  Returns 0 if successful and -1 otherwise.
// The output string is only modified if successful. The decoding can
// be done in-place.
SPARROW_API int Base64Decode(const void *input, int input_size, struct FlexibleArray **output);

enum Base64UrlEncodePolicy {
    // Include the trailing padding in the output, when necessary.
    INCLUDE_PADDING,

    // Remove the trailing padding from the output.
    OMIT_PADDING
};

// Encodes the |input| string in base64url, defined in RFC 4648:
// https://tools.ietf.org/html/rfc4648#section-5
//
// The |policy| defines whether padding should be included or omitted from the
// encoded |*output|.
SPARROW_API void Base64UrlEncode(const void *input, int input_size, Base64UrlEncodePolicy policy,
                                 struct FlexibleArray **output);

enum Base64UrlDecodePolicy {
    // Require inputs contain trailing padding if non-aligned.
    REQUIRE_PADDING,

    // Accept inputs regardless of whether or not they have the correct padding.
    IGNORE_PADDING,

    // Reject inputs if they contain any trailing padding.
    DISALLOW_PADDING
};

// Decodes the |input| string in base64url, defined in RFC 4648:
// https://tools.ietf.org/html/rfc4648#section-5
//
// The |policy| defines whether padding will be required, ignored or disallowed
// altogether. Returns 0 if successful and -1 otherwise.
SPARROW_API int Base64UrlDecode(const void *input, int input_size, Base64UrlDecodePolicy policy,
                                struct FlexibleArray **output);

#ifdef __cplusplus
} /* end extern "C" */
#endif
#endif // SPARROW_IMPL_BASE64_H_
