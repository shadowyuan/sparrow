/*
 *
 * Copyright 2015 gRPC authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "sparrow/hash.h"

#include <string.h>

#if defined(_MSC_VER)
#define BIG_CONSTANT(x) (x)
#else
#define BIG_CONSTANT(x) (x##LLU)
#endif

#define ROTL32(x, r) ((x) << (r)) | ((x) >> (32 - (r)))

#define FMIX32(h)                                                                                  \
    (h) ^= (h) >> 16;                                                                              \
    (h) *= 0x85ebca6b;                                                                             \
    (h) ^= (h) >> 13;                                                                              \
    (h) *= 0xc2b2ae35;                                                                             \
    (h) ^= (h) >> 16;

extern "C" uint32_t murmur_hash_32(const void *key, int len, uint32_t seed) {
    uint32_t h1 = seed;
    uint32_t k1;

    const uint32_t c1 = 0xcc9e2d51;
    const uint32_t c2 = 0x1b873593;

    const uint8_t *keyptr = static_cast<const uint8_t *>(key);
    constexpr int bsize = sizeof(k1);
    const int nblocks = len / bsize;

    /* body */
    for (int i = 0; i < nblocks; i++, keyptr += bsize) {
        memcpy(&k1, keyptr, bsize);

        k1 *= c1;
        k1 = ROTL32(k1, 15);
        k1 *= c2;

        h1 ^= k1;
        h1 = ROTL32(h1, 13);
        h1 = h1 * 5 + 0xe6546b64;
    }

    k1 = 0;

    /* tail */
    switch (len & 3) {
    case 3:
        k1 ^= (static_cast<uint32_t>(keyptr[2])) << 16;
    /* fallthrough */
    case 2:
        k1 ^= (static_cast<uint32_t>(keyptr[1])) << 8;
    /* fallthrough */
    case 1:
        k1 ^= keyptr[0];
        k1 *= c1;
        k1 = ROTL32(k1, 15);
        k1 *= c2;
        h1 ^= k1;
    };

    /* finalization */
    h1 ^= static_cast<uint32_t>(len);
    FMIX32(h1);
    return h1;
}

#undef ROTL32
#undef FMIX32

#if defined(__x86_64__) || defined(__amd64__) || defined(__aarch64__) || defined(_M_X64) ||        \
    defined(_M_AMD64)
//-----------------------------------------------------------------------------
// MurmurHash2, 64-bit versions, by Austin Appleby
// 64-bit hash for 64-bit platforms
extern "C" uint64_t murmur_hash_64(const void *key, int len, uint64_t seed) {
    const uint64_t m = BIG_CONSTANT(0xc6a4a7935bd1e995);
    const int r = 47;

    uint64_t h = seed ^ (len * m);

    const uint64_t *data = (const uint64_t *)key;
    const uint64_t *end = data + (len / 8);

    while (data != end) {
        uint64_t k = *data++;

        k *= m;
        k ^= k >> r;
        k *= m;

        h ^= k;
        h *= m;
    }

    const unsigned char *data2 = (const unsigned char *)data;

    switch (len & 7) {
    case 7:
        h ^= uint64_t(data2[6]) << 48;
    case 6:
        h ^= uint64_t(data2[5]) << 40;
    case 5:
        h ^= uint64_t(data2[4]) << 32;
    case 4:
        h ^= uint64_t(data2[3]) << 24;
    case 3:
        h ^= uint64_t(data2[2]) << 16;
    case 2:
        h ^= uint64_t(data2[1]) << 8;
    case 1:
        h ^= uint64_t(data2[0]);
        h *= m;
    };

    h ^= h >> r;
    h *= m;
    h ^= h >> r;

    return h;
}
#else
// 64-bit hash for 32-bit platforms
extern "C" uint64_t murmur_hash_64(const void *key, int len, uint64_t seed) {
    const uint32_t m = 0x5bd1e995;
    const int r = 24;

    uint32_t h1 = uint32_t(seed) ^ len;
    uint32_t h2 = uint32_t(seed >> 32);

    const uint32_t *data = (const uint32_t *)key;

    while (len >= 8) {
        uint32_t k1 = *data++;
        k1 *= m;
        k1 ^= k1 >> r;
        k1 *= m;
        h1 *= m;
        h1 ^= k1;
        len -= 4;

        uint32_t k2 = *data++;
        k2 *= m;
        k2 ^= k2 >> r;
        k2 *= m;
        h2 *= m;
        h2 ^= k2;
        len -= 4;
    }

    if (len >= 4) {
        uint32_t k1 = *data++;
        k1 *= m;
        k1 ^= k1 >> r;
        k1 *= m;
        h1 *= m;
        h1 ^= k1;
        len -= 4;
    }

    switch (len) {
    case 3:
        h2 ^= ((unsigned char *)data)[2] << 16;
    case 2:
        h2 ^= ((unsigned char *)data)[1] << 8;
    case 1:
        h2 ^= ((unsigned char *)data)[0];
        h2 *= m;
    };

    h1 ^= h2 >> 18;
    h1 *= m;
    h2 ^= h1 >> 22;
    h2 *= m;
    h1 ^= h2 >> 17;
    h1 *= m;
    h2 ^= h1 >> 19;
    h2 *= m;

    uint64_t h = h1;

    h = (h << 32) | h2;

    return h;
}
#endif
