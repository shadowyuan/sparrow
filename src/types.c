#include "sparrow/impl/types.h"
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

struct FlexibleArray *FlexibleArrayInit(int len) {
    struct FlexibleArray *p = malloc(len + sizeof(struct FlexibleArray) - 1);
    if (p != NULL) {
        p->length = len;
    }
    return p;
}

struct FlexibleArray *FlexibleArrayInitWithData(const void *data, int len) {
    struct FlexibleArray *p = malloc(len + sizeof(struct FlexibleArray) - 1);
    if (p != NULL) {
        memcpy(&p->data[0], data, len);
        p->length = len;
    }
    return p;
}

void FlexibleArrayFree(struct FlexibleArray *ptr) {
    if (ptr) free(ptr);
}
